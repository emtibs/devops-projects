#!/usr/bin/env groovy
def label = "zalenium-${UUID.randomUUID().toString()}"

podTemplate(label: label, containers: [
  containerTemplate(name: 'maven-firefox', image: 'maven:3.3.9-jdk-8-alpine', ttyEnabled: true, command: 'cat'),
  containerTemplate(name: 'maven-chrome', image: 'maven:3.3.9-jdk-8-alpine', ttyEnabled: true, command: 'cat'),
   containerTemplate(name: 'selenium', image: 'elgalu/selenium'),
  // because containers run in the same network space, we need to make sure there are no port conflicts
  // we also need to adapt the selenium images because they were designed to work with the --link option
  containerTemplate(name: 'zalenium', image: 'dosel/zalenium', ttyEnabled: true, command: 'start', envVars: [
    containerEnvVar(key: 'HUB_PORT_4444_TCP_ADDR', value: 'localhost'),
    containerEnvVar(key: 'HUB_PORT_4444_TCP_PORT', value: '4444'),
    containerEnvVar(key: 'DISPLAY', value: ':99.0'),
    containerEnvVar(key: 'SE_OPTS', value: '-port 5556'),
  ]),
  volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
                   hostPathVolume(hostPath: '/tmp/videos', mountPath: '/home/seluser/videos'),
                   hostPathVolume(hostPath: '/usr/bin/docker', mountPath: '/usr/bin/docker'),
  ]
  ]) {

  node(label) {
    stage('Checkout') {
      git 'https://gitlab.com/emtibs/devops-projects.git'
      parallel (
        firefox: {
          container('maven-firefox') {
            stage('Test firefox') {
              sh 'mvn -f bpi/web-ui-automation-best-practices/pom.xml -B clean test -Dselenium.browser=firefox -Dsurefire.rerunFailingTestsCount=5 -Dsleep=0'
            }
          }
        },
        chrome: {
          container('maven-chrome') {
            stage('Test chrome') {
              sh 'mvn -f bpi/web-ui-automation-best-practices/pom.xml -B clean test -Dselenium.browser=chrome -Dsurefire.rerunFailingTestsCount=5 -Dsleep=0'
            }
          }
        }
      )
    }

    stage('Logs') {
      containerLog('selenium-chrome')
      containerLog('selenium-firefox')
    }
  }
}
