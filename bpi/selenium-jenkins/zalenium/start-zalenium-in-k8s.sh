#!/usr/bin/env bash

kubectl create namespace zalenium

kubectl --namespace=zalenium create -f zalenium-service.yaml
kubectl --namespace=zalenium create -f zalenium-cluster-role-binding.yaml
kubectl --namespace=zalenium create -f zalenium-deployment.yaml

kubectl get all,sa,roles,rolebindings,pvc,pv --namespace zalenium